/* Module data structure */

// moduleName: {
//     dataType: {
//         property: value
//     }
// }

/* Module data example */

_template: {
    big: {
        title: 'Hello world',
        age: 10,
        button: false
    }
},

head: {
    defaults: {
        title: 'Welcome to ANN Studio',
        useSocialMetaTags: true
    }
},

"slider": {
    "defaults" :{
        "bg" : "#E9E9E9"
    },
    "sliders": [
        {
            "title":"Slider 1",
            "href" : 'javascript:void(0)',
            "data" : {
                "data-transition" :"fade",
                "data-slotamount" : "7",
                "data-masterspeed" : "900",
                "data-saveperformance" : "off"
            },
            "image" :{
                "image" : "static/img/content/kenburns6.jpg",
                "thumb" : "static/img/content/kenburns6.jpg",
                "data" : {
                    "data-bgposition" : "center top",
                    "data-bgfit" :"cover",
                    "data-bgrepeat" : "no-repeat"
                }

            },
            "texts" : {
                "big" : {
                    "text" : "Welcome to <a href='' style='color:#f9bf3b'>Amass Studio</a>",

                    "data" : {
                        "data-x" : "center",
                        "data-hoffset" : "0",
                        "data-y" : "center",
                        "data-voffset" : "-200",
                        "data-speed" : "900",
                        "data-start" : "1000",
                        "data-easing" : "easeOutExpo",
                        "data-splitin" : "none",
                        "data-splitout" : "none",
                        "data-elementdelay" : "0.1",
                        "data-endelementdelay" : "0.1",
                        "data-endspeed" : "900"
                    }
                },
                "small" : {
                    "text" : "solution for Web Designer",
                    "data" : {
                        "data-x" : "center",
                        "data-hoffset" : "0",
                        "data-y" : "center",
                        "data-voffset" : "0",
                        "data-speed" : "900",
                        "data-start" : "1500",
                        "data-easing" : "easeOutExpo",
                        "data-splitin" : "none",
                        "data-splitout" : "none",
                        "data-elementdelay" : "0.1",
                        "data-endelementdelay" : "0.1",
                        "data-endspeed" : "900"
                    }
                },
                "button" : {
                    "text" : "<a href='#' class='button'>PURCHASE NOW</a>",
                    "data" : {
                        "data-transform_in" : "y:50px;opacity:0;s:1200;e:Power2.easeInOut;",
                        "data-transform_out" : "y:-50px;opacity:0;s:500;s:500;",
                        "data-responsive_offset" : "on",
                        "data-x" : "center",
                        "data-hoffset" : "0",
                        "data-y" : "center",
                        "data-voffset" : "200",
                        "data-speed" : "900",
                        "data-start" : "2600",
                        "data-easing" : "easeOutExpo",
                        "data-splitin" : "none",
                        "data-splitout" : "none",
                        "data-elementdelay" : "0.1",
                        "data-endelementdelay" : "0.1",
                        "data-endspeed" : "900"
                     }
                }
            }
        },

        {
            "title":"Slider 2",
            "href" : 'javascript:void(0)',
            "data" : {
                "data-transition" :"fade",
                "data-slotamount" : "7",
                "data-masterspeed" : "900",
                "data-saveperformance" : "off"
            },
            "image" :{
                "image" : "static/img/content/kenburns1.jpg",
                "thumb" : "static/img/content/kenburns1.jpg",
                "data" : {
                    "data-bgposition" : "center top",
                    "data-bgfit" :"cover",
                    "data-bgrepeat" : "no-repeat"
                }

            },
            "texts" : {
                "big" : {
                    "text" : "Welcome to <span style='color:#f9bf3b'>Amass Studio</span>",

                    "data" : {
                        "data-x" : "center",
                        "data-hoffset" : "0",
                        "data-y" : "center",
                        "data-voffset" : "-200",
                        "data-speed" : "900",
                        "data-start" : "1000",
                        "data-easing" : "easeOutExpo",
                        "data-splitin" : "none",
                        "data-splitout" : "none",
                        "data-elementdelay" : "0.1",
                        "data-endelementdelay" : "0.1",
                        "data-endspeed" : "900"
                    }
                },
                "small" : {
                    "text" : "solution for Web Designer",
                    "data" : {
                        "data-x" : "center",
                        "data-hoffset" : "0",
                        "data-y" : "center",
                        "data-voffset" : "0",
                        "data-speed" : "900",
                        "data-start" : "1500",
                        "data-easing" : "easeOutExpo",
                        "data-splitin" : "none",
                        "data-splitout" : "none",
                        "data-elementdelay" : "0.1",
                        "data-endelementdelay" : "0.1",
                        "data-endspeed" : "900"
                    }
                },
                "button" : {
                    "text" : "<a href='#' class='button'>PURCHASE NOW</a>",
                    "data" : {
                        "data-transform_in" : "y:50px;opacity:0;s:1200;e:Power2.easeInOut;",
                        "data-transform_out" : "y:-50px;opacity:0;s:500;s:500;",
                        "data-responsive_offset" : "on",
                        "data-x" : "center",
                        "data-hoffset" : "0",
                        "data-y" : "center",
                        "data-voffset" : "200",
                        "data-speed" : "900",
                        "data-start" : "2600",
                        "data-easing" : "easeOutExpo",
                        "data-splitin" : "none",
                        "data-splitout" : "none",
                        "data-elementdelay" : "0.1",
                        "data-endelementdelay" : "0.1",
                        "data-endspeed" : "900"
                    }
                }
            }
        },

        {
            "title":"Slider 3",
            "href" : 'javascript:void(0)',
            "data" : {
                "data-transition" :"fade",
                "data-slotamount" : "7",
                "data-masterspeed" : "900",
                "data-saveperformance" : "off"
            },
            "image" :{
                "image" : "static/img/content/kenburns3.jpg",
                "thumb" : "static/img/content/kenburns3.jpg",
                "data" : {
                    "data-bgposition" : "center top",
                    "data-bgfit" :"cover",
                    "data-bgrepeat" : "no-repeat"
                }

            },
            "texts" : {
                "big" : {
                    "text" : "Welcome to <span style='color:#f9bf3b'>Amass Studio</span>",

                    "data" : {
                        "data-x" : "center",
                        "data-hoffset" : "0",
                        "data-y" : "center",
                        "data-voffset" : "-200",
                        "data-speed" : "900",
                        "data-start" : "1000",
                        "data-easing" : "easeOutExpo",
                        "data-splitin" : "none",
                        "data-splitout" : "none",
                        "data-elementdelay" : "0.1",
                        "data-endelementdelay" : "0.1",
                        "data-endspeed" : "900"
                    }
                },
                "small" : {
                    "text" : "solution for Web Designer",
                    "data" : {
                        "data-x" : "center",
                        "data-hoffset" : "0",
                        "data-y" : "center",
                        "data-voffset" : "0",
                        "data-speed" : "900",
                        "data-start" : "1500",
                        "data-easing" : "easeOutExpo",
                        "data-splitin" : "none",
                        "data-splitout" : "none",
                        "data-elementdelay" : "0.1",
                        "data-endelementdelay" : "0.1",
                        "data-endspeed" : "900"
                    }
                },
                "button" : {
                    "text" : "<a href='#' class='button'>PURCHASE NOW</a>",
                    "data" : {
                        "data-transform_in" : "y:50px;opacity:0;s:1200;e:Power2.easeInOut;",
                        "data-transform_out" : "y:-50px;opacity:0;s:500;s:500;",
                        "data-responsive_offset" : "on",
                        "data-x" : "center",
                        "data-hoffset" : "0",
                        "data-y" : "center",
                        "data-voffset" : "200",
                        "data-speed" : "900",
                        "data-start" : "2600",
                        "data-easing" : "easeOutExpo",
                        "data-splitin" : "none",
                        "data-splitout" : "none",
                        "data-elementdelay" : "0.1",
                        "data-endelementdelay" : "0.1",
                        "data-endspeed" : "900"
                    }
                }
            }
        },
    ]
}
,

"socials": {
    "socials": [
        {"title":"twitter", "href" : "javascript:void(0)", "class" : "twitter"},
        {"title":"facebook", "href" : "javascript:void(0)", "class" : "facebook"},
        {"title":"google", "href" : "javascript:void(0)", "class" : "google"},
        {"title":"instagram", "href" : "javascript:void(0)", "class" : "instagram"},
        {"title":"pinterest", "href" : "javascript:void(0)", "class" : "pinterest"}
    ]
}
,

"main_menu": {
    "menu_list": [
        {
            "title":"Home", "href" : "javascript:void(0)"
        },
        {
            "title":"About us", "href" : "javascript:void(0)"
        },
        {
            "title":"Services", "href" : "javascript:void(0)"
        },
        {
            "title":"Work", "href" : "javascript:void(0)"
        },
        {
            "title":"Our team", "href" : "javascript:void(0)"
        },
        {
            "title":"News & Blog", "href" : "javascript:void(0)"
        },
        {
            "title":"Contact", "href" : "javascript:void(0)"
        }
    ]
}
